package com.itmuch.cloud.study.user.entity;

import lombok.Data;

@Data
public class BlogData
{
    private Record data;
}
