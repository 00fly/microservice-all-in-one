package com.itmuch.cloud.study.user.service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.itmuch.cloud.study.core.utils.JsonBeanUtils;
import com.itmuch.cloud.study.user.entity.Article;
import com.itmuch.cloud.study.user.entity.BlogData;

import lombok.extern.slf4j.Slf4j;

/**
 * DataService
 */
@Slf4j
@Service
public class DataService
{
    WebClient webClient = WebClient.builder().codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(-1)).build();
    
    /**
     * 获取Article数据列表
     * 
     * @return
     * @throws IOException
     */
    @Cacheable(cacheNames = "data", key = "'articles'", sync = true)
    public List<Article> getArticles()
        throws IOException
    {
        log.info("★★★★★★★★ getData from webApi ★★★★★★★★");
        String resp = webClient.get().uri("https://00fly.online/upload/data.json").acceptCharset(StandardCharsets.UTF_8).accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(String.class).block();
        return JsonBeanUtils.jsonToBean(resp, BlogData.class, true).getData().getList();
    }
}