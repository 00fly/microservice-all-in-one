package com.itmuch.cloud.study.user.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.itmuch.cloud.study.user.entity.User;

@FeignClient(name = "microservice-ribbon-user", url = "${user.api.url:127.0.0.1:8081}", fallback = FeignClientFallback.class)
public interface UserFeignClient
{
    @GetMapping("/{id}")
    public User findById(@PathVariable("id") Long id);
}

/**
 * 回退类FeignClientFallback需实现Feign Client接口，FeignClientFallback也可以是public class，没有区别
 * 
 */
@Component
class FeignClientFallback implements UserFeignClient
{
    @Override
    public User findById(Long id)
    {
        User user = new User();
        user.setId(-1L);
        user.setUsername("默认用户");
        return user;
    }
}