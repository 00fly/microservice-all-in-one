# ribbon-all-in-one

## 一，功能介绍

脱离eureka使用ribbon，显式的设置负载均衡规则和地址。


RibbonConfiguration或yml文件配置负载均衡规则（二选一）

```java
@Configuration
public class RibbonConfiguration
{
    @Bean
    IRule ribbonRule()
    {
        // 负载均衡规则，改为随机
        return new RandomRule();
    }
}

/**
 * 使用RibbonClient，为特定name的Ribbon Client自定义配置. 使用@RibbonClient的configuration属性，指定Ribbon的配置类. <br>
 * 可参考的示例： http://spring.io/guides/gs/client-side-load-balancing/
 * 
 */
@Configuration
@RibbonClient(name = "microservice-ribbon-user", configuration = RibbonConfiguration.class)
public class TestConfiguration
{
}

```

application.yml 配置负载均衡地址

```yml
#设置负载均衡参数
microservice-ribbon-user:
  ribbon:
    #配置规则
    NFLoadBalancerRuleClassName: com.netflix.loadbalancer.RandomRule
    
    #配置地址：宿主机ip+映射端口或docker自定义网络指定地址
    #listOfServers: 172.22.208.1:8080,172.22.208.1:8081
    listOfServers: 172.88.88.100:8081,172.88.88.101:8081
```

### 负载均衡策略
- RandomRule 实现从服务实例清单中随机选择一个服务实例的功能。
- RoundRobinRule 实现了按照线性轮询的方式依次选择每个服务实例的功能。
- RetryRule 实现了一个具备重试机制的实例选择功能。
- WeightedResponseTimeRule是对 RoundRobinRule 的拓展，增加了根据实例的运行情况来计算权重，并根据权重来挑选实例。
- ClientConfigEnableRoundRobinRule 通过继承该策略，在子类中做一些高级策略时有可能会存在一些无法实施的情况，那么就可以用父类的实现作为备选（线性轮询机制）。
- BestAvailableRule 通过遍历负载均衡器中维护的所有服务实例，会过滤掉故障的实例，并找出并发请求数最小的一个，所以该策略的特性是可选出最空闲的实例。
- PredicateBasedRule 先通过子类实现中的 Predicate 逻辑来过滤一部分服务实例，然后再以线性轮询的方式从过滤后的实例清单中选出一个。
- AvailabilityFilteringRule 通过线性抽样的方式直接尝试寻找可用且较空闲的实例来使用。
- ZoneAvoidanceRule

负载均衡策略有很多苛刻的条件，如果出现了一些很奇怪的现象的话，建议查看源码。

## 二，调用方向

```mermaid
graph LR
C1(服务网关gateway)
D1(微服务movie)
E1(微服务user)
E2(微服务user)

 C1 --> D1 --> E1
		D1 --> E2
 C1 --> E1
 C1 --> E2

```


## 三，如何运行

镜像已经上传阿里云：

```
registry.cn-shanghai.aliyuncs.com/00fly/ribbon-gateway:0.0.1

registry.cn-shanghai.aliyuncs.com/00fly/ribbon-user:0.0.1

registry.cn-shanghai.aliyuncs.com/00fly/ribbon-movie:0.0.1
```

下载docker目录文件上传到lunix服务器，依次执行下面的脚本命令

```shell
cd docker

sh restart.sh
```

浏览器输入以下地址查看调试页面

- 网关接口： http://{宿主机ip}:8080/doc.html
     
功能演示地址 [http://124.71.129.204:8085/doc.html](http://124.71.129.204:8085/doc.html)


## 四、链路跟踪

需要先启动zipkin-server

**SpringBoot 2.0以上版本已经不需要自行搭建zipkin-server**

地址 [https://repo1.maven.org/maven2/io/zipkin/zipkin-server/](https://repo1.maven.org/maven2/io/zipkin/zipkin-server/) 下载exec.jar


Jar运行，默认9411端口

 ```shell
 java -jar zipkin-server-2.20.0-exec.jar
 
 java -jar zipkin-server-2.20.0-exec.jar --server.port=8087
 ```
