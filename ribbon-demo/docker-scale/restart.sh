#!/bin/bash
docker-compose down && \
docker-compose --compatibility up -d --scale ribbon-user=2 --scale ribbon-movie=2 && \
docker stats