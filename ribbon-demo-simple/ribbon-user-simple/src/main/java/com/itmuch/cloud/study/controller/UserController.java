package com.itmuch.cloud.study.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.itmuch.cloud.study.entity.User;
import com.itmuch.cloud.study.repository.UserRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "user模块")
@RestController
public class UserController
{
    @Autowired
    private UserRepository userRepository;
    
    @ApiOperation("查询用户")
    @GetMapping("/{id:\\d+}")
    public Optional<User> findById(@PathVariable Long id)
    {
        return this.userRepository.findById(id);
    }
    
    @ApiOperation("查询全部用户")
    @GetMapping("getAll")
    public List<User> getAll()
    {
        return this.userRepository.findAll();
    }
}
