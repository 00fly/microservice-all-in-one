package com.itmuch.cloud.study;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.itmuch.cloud.study.entity.User;
import com.itmuch.cloud.study.repository.UserRepository;

@SpringBootApplication
public class ProviderUserApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(ProviderUserApplication.class, args);
    }
    
    @Value("${server.port}")
    Integer port;
    
    /**
     * 初始化用户信息 注：Spring Boot2不能像1.x一样，用spring.datasource.schema/data指定初始化SQL脚本，否则与actuator不能共存<br>
     * 原因：https://github.com/spring-projects/spring-boot/issues/13042<br>
     * https://github.com/spring-projects/spring-boot/issues/13539
     *
     * @param repository repo
     * @return runner
     */
    @Bean
    ApplicationRunner init(UserRepository repository)
    {
        return args -> {
            String ip = InetAddress.getLocalHost().getHostAddress();
            int init = (int)(System.currentTimeMillis() % 10);
            User user1 = new User(1L, "account1", String.format("张三 from %s:%s", ip, port), init + 20, new BigDecimal(100.00));
            User user2 = new User(2L, "account2", String.format("李四 from %s:%s", ip, port), init + 30, new BigDecimal(180.00));
            User user3 = new User(3L, "account3", String.format("王五 from %s:%s", ip, port), init + 40, new BigDecimal(280.00));
            Stream.of(user1, user2, user3).forEach(repository::save);
        };
    }
}
