package com.itmuch.cloud.study.user.entity;

import lombok.Data;

@Data
public class Article
{
    String title;
    
    String url;
    
    Long viewCount;
}
