#!/bin/bash
docker-compose -f base.yml -p base down
docker-compose -f core-web.yml -p web down
docker-compose -f gateway.yml -p gateway down
docker-compose -f front.yml -p front down