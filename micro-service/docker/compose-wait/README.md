# microservice-all-in-one工程docker部署

## 一, 如何控制服务启动顺序

- **等等我**  借助wait-for.sh设置启动前提条件


### 启动顺序

1. microservice-eureka-1

2. microservice-user

3. microservice-movie

4. microservice-gateway

5. microservice-front
