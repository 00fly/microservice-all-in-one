# microservice-all-in-one工程docker部署

## 单服务器上应用做水平拓展

### 1. 修改docker-compose文件，去掉 `microservice-user、microservice-movie` 多实例部署会导致冲突的container_name、ports节点内容


### 2. 启动服务

`docker-compose --compatibility up -d --scale microservice-user=2`

`docker-compose --compatibility up -d --scale microservice-user=2 --scale microservice-movie=2`


### 3. 整体架构

```mermaid
graph LR
A(前端工程front) 
B(流量网关nginx)
C1(服务网关gateway)
C2(服务网关gateway)
D1(微服务movie)
D2(微服务movie)
E1(微服务user)
E2(微服务user)
ER1(服务注册中心1)
ER2(服务注册中心2)

A --> B
B --> C1
B --> C2
        C1 --> D1
        C1 --> D2
        C2 --> D1	
        C2 --> D2
                D1 --> E1
                D1 --> E2
                D2 --> E1
                D2 --> E2
        C1 --> E1
        C1 --> E2
        C2 --> E1
        C2 --> E2

```
### 4. 疑问

**流量网关nginx、服务网关gateway实际是怎么部署的？**

流量网关nginx应该不推荐使用docker部署，那么服务网关gateway是不是同eureka类似，规划好实例数量并获取ip地址、宿主机对应的开放端口等参数，配置到nginx？
