# microservice-all-in-one工程docker部署

## 一, 如何控制服务启动顺序

 1.**等等我**  借助wait-for.sh设置前提判断


 2.**各走各路** 分组启动，必须保证服务在同一网络内


## 二，开发小纪

- 已完成compose模式wait、step-by-step、network-step-by-step 调试

- compose模式同一台服务器水平扩容，参考：compose-scale

- 待调试stack模式