package com.fly.cloud.study;

import java.io.IOException;
import java.net.InetAddress;

import org.apache.commons.lang.SystemUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 使用Eureka做服务发现.
 * 
 * @author 周立
 */
@EnableConfigServer
@EnableEurekaClient
@SpringBootApplication
public class ConfigServerApplication implements CommandLineRunner
{
    public static void main(String[] args)
    {
        SpringApplication.run(ConfigServerApplication.class, args);
    }
    
    @Value("${server.port}")
    Integer port;
    
    @Override
    public void run(String... args)
    {
        try
        {
            if (SystemUtils.IS_OS_WINDOWS)// 防止非windows系统报错，启动失败
            {
                String ip = InetAddress.getLocalHost().getHostAddress();
                String url = "http://" + ip + ":" + port;
                Runtime.getRuntime().exec("cmd /c start " + url + "/microservice-movie-dev.yml");
                Runtime.getRuntime().exec("cmd /c start " + url + "/microservice-user-dev.yml");
            }
        }
        catch (IOException e)
        {
        }
    }
}
