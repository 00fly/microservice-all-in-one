# 基于 Spring Cloud 的微服务架构
 
 
## 一、快速启动项目

* 使用 Docker 快速启动
    1. 配置 Docker 环境
    2. `mvn clean package` 打包项目及 Docker 镜像
    3. 在项目根目录下执行 `docker-compose up -d` 启动所有项目
    
    
* 本地手动启动
    1. 配置 rabbitmq
    
    2. 修改 hosts 将主机名指向到本地   
       `127.0.0.1	microservice-eureka-1 microservice-eureka-2 microservice-config microservice-movie microservice-user` 
       
    3. 按顺序启动各服务
   
    