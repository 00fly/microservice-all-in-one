package com.fly.cloud.study.user.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.fly.cloud.study.user.entity.User;

@FeignClient(name = "microservice-user")
public interface UserFeignClient
{
    @GetMapping("/{id}")
    public User findById(@PathVariable("id") Long id);
}
