package com.fly.cloud.study.user.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fly.cloud.study.user.entity.User;
import com.fly.cloud.study.user.feign.UserFeignClient;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "movie模块")
@RestController
public class MovieController
{
    String serverIp;
    
    @Autowired
    private UserFeignClient userFeignClient;
    
    @PostConstruct
    private void init()
    {
        try
        {
            serverIp = InetAddress.getLocalHost().getHostAddress();
        }
        catch (UnknownHostException e)
        {
        }
    }
    
    @ApiOperation("查询用户")
    @GetMapping("/user/{id}")
    public User findById(@PathVariable Long id)
    {
        // 带出serverIp方便判断数据来源容器
        User user = this.userFeignClient.findById(id);
        user.setName(user.getName() + " === in server：" + serverIp);
        return user;
    }
}
