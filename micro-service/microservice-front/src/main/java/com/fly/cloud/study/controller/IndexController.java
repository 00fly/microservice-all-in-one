package com.fly.cloud.study.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController
{
    @Value("${gateway.ip:}")
    private String ip;
    
    @GetMapping({"/", "/index"})
    public String index(Model model)
        throws UnknownHostException
    {
        if (StringUtils.isBlank(ip))
        {
            // docker部署取值为容器ip
            ip = InetAddress.getLocalHost().getHostAddress();
        }
        model.addAttribute("baseUrl", "http://" + ip);
        return "index";
    }
}
