# micro-service

## 一，介绍

微服务示例，持续优化

一般而言，微服务是针对前后端分离开发项目，有单独的前端工程，后台WebApi使用微服务架构



## 二，模块划分

| 模块                     |   功能   |        说明        |
| :----------------------- | :------: | :----------------: |
| **microservice-eureka**  | 注册中心 |         -          |
| **microservice-front**   | 前端工程 |  后台访问网关接口  |
| **microservice-gateway** |   网关   | 路由、聚合接口文档 |
| **microservice-movie**   | 微服务1  |         -          |
| **microservice-user**    | 微服务2  |         -          |


## 三，SpringCloud 五大核心组件

### 1. Eureka 注册中心  

### 2. Ribbon/Feign 负载均衡 

### 3. Zuul/GateWay 网关

### 4. Config 配置中心

### 5. Hystrix 熔断器


## 四，整体架构

```mermaid
graph LR
A(前端工程front) 
B(流量网关nginx)
C1(服务网关gateway)
C2(服务网关gateway)
D1(微服务movie)
D2(微服务movie)
E1(微服务user)
E2(微服务user)
ER1(服务注册中心1)
ER2(服务注册中心2)

A --> B
B --> C1
B --> C2
        C1 --> D1
        C1 --> D2
        C2 --> D1	
        C2 --> D2
                D1 --> E1
                D1 --> E2
                D2 --> E1
                D2 --> E2
        C1 --> E1
        C1 --> E2
        C2 --> E1
        C2 --> E2

```