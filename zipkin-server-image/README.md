# zipkin-server-image

**SpringBoot 2.0以上版本已经不需要自行搭建zipkin-server**

Zipkin官网：[http://zipkin.io/](http://zipkin.io)

我们可以从以下地址下载exec.jar

【版本 0.4.3-2.12.9】[https://repo1.maven.org/maven2/io/zipkin/java/zipkin-server/](https://repo1.maven.org/maven2/io/zipkin/java/zipkin-server/)

【版本 2.14.1-3.4.2】[https://repo1.maven.org/maven2/io/zipkin/zipkin-server/](https://repo1.maven.org/maven2/io/zipkin/zipkin-server)

`从2.18.0版本开始提供了slim版本，可选择使用`

Slim版本是指软件或硬件的一种瘦身版本，它相比于完整版或标准版，体积更小，功能可能有所精简。




## 一、Jar运行 

**1. 使用rabbitmq收集数据启动Zipkin命令示例：**

 ```shell
 java -jar zipkin-server-2.20.0-exec.jar 
--zipkin.collector.rabbitmq.addresses=192.168.0.1
--zipkin.collector.rabbitmq.username=账号
--zipkin.collector.rabbitmq.password=密码
 ```

**2. 修改启动端口**

 ```shell
 java -jar zipkin-server-2.20.0-exec.jar --server.port=8087
 ```


## 二、Docker镜像运行

 1. 依次执行`1st-wget-zipkin.sh、2nd-build.sh` 实现下载可执行jar、镜像打包。

 2. 执行`restart.sh、stop.sh` 重启、停止容器。


或者使用官方镜像运行 `docker run -d -p 9411:9411 openzipkin/zipkin`